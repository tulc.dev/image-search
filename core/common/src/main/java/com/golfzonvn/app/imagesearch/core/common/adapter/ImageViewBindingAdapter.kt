package com.golfzonvn.app.imagesearch.core.common.adapter

import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.golfzonvn.app.imageloader.ImageLoader
import com.golfzonvn.app.imageloader.R

@BindingAdapter("imageUrl", "placeholder", requireAll = false)
fun ImageView.loadUrl(url: String?, placeholder: Int?) {
    if (url.isNullOrBlank()) {
        setImageDrawable(null)
        return
    }

    ImageLoader.with(this).load(url).placeholder(placeholder).into(this)
}