package com.golfzonvn.app.imagesearch.core.common.extensions

import android.app.Activity
import android.graphics.Color
import android.view.View
import android.view.WindowInsetsController


fun Activity.setFullScreen() {
    var newUiOptions = window?.decorView?.systemUiVisibility
        ?: (View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_FULLSCREEN)

    newUiOptions = newUiOptions or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

    // show navigation
    newUiOptions = newUiOptions or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    window?.statusBarColor = Color.TRANSPARENT

    window?.decorView?.systemUiVisibility = newUiOptions
}
