package com.golfzonvn.app.imagesearch.core.common.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

fun Fragment.hideKeyboard() {
    if (!isAdded) return
    val ctx = context ?: return
    view?.windowToken?.let { token ->
        val imm = ctx.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(token, 0)
    }
}