package com.golfzonvn.app.imagesearch.core.data


interface Synchronizer {
    /**
     * Syntactic sugar to call [Syncable.syncWith] while omitting the synchronizer argument
     */
    suspend fun Syncable.sync() = this@sync.syncWith(this@Synchronizer)
}

interface Syncable  {
    suspend fun syncWith(synchronizer: Synchronizer): Boolean
}