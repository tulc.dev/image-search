package com.golfzonvn.app.imagesearch.core.data.di

import com.golfzonvn.app.imagesearch.core.data.repository.ImageRepository
import com.golfzonvn.app.imagesearch.core.data.repository.ImageRepositoryImpl
import com.golfzonvn.app.imagesearch.core.data.repository.UserDataRepository
import com.golfzonvn.app.imagesearch.core.data.repository.UserDataRepositoryImpl
import com.golfzonvn.app.imagesearch.core.network.ImageSearchNetworkDataSource
import com.golfzonvn.app.imagesearch.core.network.RetrofitImageSearchNetwork
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface DataModule {

    @Binds
    fun provideImageSearchNetworkDataSource(
        datasource: RetrofitImageSearchNetwork
    ): ImageSearchNetworkDataSource

    @Binds
    fun provideImageRepository(
        repository: ImageRepositoryImpl
    ): ImageRepository

    @Binds
    fun provideUserDataRepository(
        repository: UserDataRepositoryImpl
    ): UserDataRepository
}