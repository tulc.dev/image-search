package com.golfzonvn.app.imagesearch.core.data.model

import com.golfzonvn.app.imagesearch.core.database.model.ImageEntity
import com.golfzonvn.app.imagesearch.core.model.Image
import com.golfzonvn.app.imagesearch.core.network.model.ImageNetwork

fun ImageEntity.asImage() = Image(
    id = this.id,
    photographer = this.photographer,
    url = this.url,
    thumbnail = this.thumbnail,
)