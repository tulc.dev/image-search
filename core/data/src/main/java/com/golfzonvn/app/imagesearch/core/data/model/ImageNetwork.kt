package com.golfzonvn.app.imagesearch.core.data.model

import com.golfzonvn.app.imagesearch.core.database.model.ImageEntity
import com.golfzonvn.app.imagesearch.core.model.Image
import com.golfzonvn.app.imagesearch.core.network.model.ImageNetwork

fun ImageNetwork.asImage() = Image(
    id = this.id,
    photographer = this.photographer,
    url = this.src?.original,
    thumbnail = this.src?.medium,
)

fun ImageNetwork.asImageEntity() = ImageEntity(
    id = this.id,
    photographer = this.photographer,
    url = this.src?.original,
    thumbnail = this.src?.medium,
    modified = System.currentTimeMillis(),
)