package com.golfzonvn.app.imagesearch.core.data.repository

import com.golfzonvn.app.imagesearch.core.data.Syncable
import com.golfzonvn.app.imagesearch.core.model.Image
import com.golfzonvn.app.imagesearch.core.network.model.ImageNetworkResponse
import kotlinx.coroutines.flow.Flow
import com.golfzonvn.app.imagesearch.core.common.model.Result

interface ImageRepository : Syncable {

    fun getImages(): Flow<List<Image>>

    suspend fun deleteAllImage()

    suspend fun removeImage(item: Image)

    fun fetchImages(query: String, page: Int = 1, perPage: Int = 15): Flow<ImageNetworkResponse>

}
