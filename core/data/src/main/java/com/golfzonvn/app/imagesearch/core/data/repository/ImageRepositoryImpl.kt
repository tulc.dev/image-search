package com.golfzonvn.app.imagesearch.core.data.repository

import com.golfzonvn.app.imagesearch.core.common.Dispatcher
import com.golfzonvn.app.imagesearch.core.common.ImageSearchDispatcher
import com.golfzonvn.app.imagesearch.core.data.Synchronizer
import com.golfzonvn.app.imagesearch.core.data.model.asImage
import com.golfzonvn.app.imagesearch.core.data.model.asImageEntity
import com.golfzonvn.app.imagesearch.core.database.dao.ImageDao
import com.golfzonvn.app.imagesearch.core.datastore.ImageSearchPreferencesDataSource
import com.golfzonvn.app.imagesearch.core.model.Image
import com.golfzonvn.app.imagesearch.core.network.ImageSearchNetworkDataSource
import com.golfzonvn.app.imagesearch.core.network.model.ImageNetworkResponse
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.flow.*


@Singleton
class ImageRepositoryImpl @Inject constructor(
    @Dispatcher(ImageSearchDispatcher.IO) private val ioDispatcher: CoroutineDispatcher,
    private val networkDataSource: ImageSearchNetworkDataSource,
    private val preferencesDataSource: ImageSearchPreferencesDataSource,
    private val imageDao: ImageDao,
) : ImageRepository {

    override fun getImages(): Flow<List<Image>> = imageDao.getImageEntities().map { list ->
        list.map {
            it.asImage()
        }
    }

    override suspend fun deleteAllImage() = imageDao.deleteAllImages()

    override suspend fun removeImage(item: Image)  {
        imageDao.deleteImages(mutableListOf(item.id))
        preferencesDataSource.deleteImage(item)
    }

    override fun fetchImages(query: String, page: Int, perPage: Int): Flow<ImageNetworkResponse> =
        flow {
            val deletedImageIds =
                preferencesDataSource.getDeletedImages().firstOrNull()?.toSet() ?: emptySet()

            val result = networkDataSource.getImages(query, page, perPage)
            val photos = networkDataSource.getImages(query, page, perPage).photos?.filter {
                !deletedImageIds.contains(it.id)
            } ?: emptyList()

            if (page == 1) {
                imageDao.deleteAllImages()
            }
            imageDao.insertOrIgnoreImages(photos.map {
                it.asImageEntity()
            })

            emit(result)
        }.flowOn(ioDispatcher)

    override suspend fun syncWith(synchronizer: Synchronizer): Boolean = true

}