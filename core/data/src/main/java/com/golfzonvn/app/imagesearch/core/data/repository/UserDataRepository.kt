package com.golfzonvn.app.imagesearch.core.data.repository

import com.golfzonvn.app.imagesearch.core.data.Syncable
import com.golfzonvn.app.imagesearch.core.model.Image
import kotlinx.coroutines.flow.Flow

interface UserDataRepository : Syncable {

    suspend fun setIndexImage(index: Int)

    fun getIndexImage(): Flow<Int>
}
