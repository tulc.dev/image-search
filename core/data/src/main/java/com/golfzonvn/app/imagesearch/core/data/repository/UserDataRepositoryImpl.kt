package com.golfzonvn.app.imagesearch.core.data.repository

import android.util.Log
import com.golfzonvn.app.imagesearch.core.common.Dispatcher
import com.golfzonvn.app.imagesearch.core.common.ImageSearchDispatcher
import com.golfzonvn.app.imagesearch.core.data.Synchronizer
import com.golfzonvn.app.imagesearch.core.data.model.asImage
import com.golfzonvn.app.imagesearch.core.data.model.asImageEntity
import com.golfzonvn.app.imagesearch.core.database.dao.ImageDao
import com.golfzonvn.app.imagesearch.core.datastore.ImageSearchPreferencesDataSource
import com.golfzonvn.app.imagesearch.core.model.Image
import com.golfzonvn.app.imagesearch.core.network.ImageSearchNetworkDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class UserDataRepositoryImpl @Inject constructor(
    @Dispatcher(ImageSearchDispatcher.IO) private val ioDispatcher: CoroutineDispatcher,
    private val preferencesDataSource: ImageSearchPreferencesDataSource,
) : UserDataRepository {

    override suspend fun setIndexImage(index: Int) = preferencesDataSource.setIndexImage(index)

    override fun getIndexImage(): Flow<Int> = preferencesDataSource.getIndexImage()

    override suspend fun syncWith(synchronizer: Synchronizer): Boolean = true

}