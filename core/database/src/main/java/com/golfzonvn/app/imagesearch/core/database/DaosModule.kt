package com.golfzonvn.app.imagesearch.core.database

import com.golfzonvn.app.imagesearch.core.database.dao.ImageDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
object DaosModule {
    @Provides
    fun providesImageDao(
        database: ImageDatabase,
    ): ImageDao = database.imageDao()
}