package com.golfzonvn.app.imagesearch.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.golfzonvn.app.imagesearch.core.database.dao.ImageDao
import com.golfzonvn.app.imagesearch.core.database.model.ImageEntity

@Database(
    entities = [ImageEntity::class],
    version = 1,
    exportSchema = true,
)
abstract class ImageDatabase : RoomDatabase() {
    abstract fun imageDao(): ImageDao
}