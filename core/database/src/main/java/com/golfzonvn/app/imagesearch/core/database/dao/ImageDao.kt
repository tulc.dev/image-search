package com.golfzonvn.app.imagesearch.core.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.golfzonvn.app.imagesearch.core.database.model.ImageEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ImageDao {
    @Query(value = "SELECT * FROM images ORDER BY modified")
    fun getImageEntities(): Flow<List<ImageEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertOrIgnoreImages(entities: List<ImageEntity>): List<Long>

    @Query(
        value = """
            DELETE FROM images
            WHERE id in (:ids)
        """,
    )
    suspend fun deleteImages(ids: List<Long>)

    @Query(value = "DELETE FROM images")
    suspend fun deleteAllImages()
}