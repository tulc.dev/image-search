package com.golfzonvn.app.imagesearch.core.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "images")
data class ImageEntity(
    @ColumnInfo("id") @PrimaryKey val id: Long,
    @ColumnInfo("photographer") val photographer: String,
    @ColumnInfo("url") val url: String?,
    @ColumnInfo("thumbnail") val thumbnail: String?,
    @ColumnInfo("modified") val modified: Long,
)