package com.golfzonvn.app.imagesearch.core.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringSetPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.golfzonvn.app.imagesearch.core.model.Image
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

interface ImageSearchPreferencesDataSource {
    fun getDeletedImages(): Flow<List<Long>>

    suspend fun deleteImage(image: Image)

    fun getIndexImage(): Flow<Int>

    suspend fun setIndexImage(index: Int)
}

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "image_preferences")

@Singleton
class ImageSearchPreferencesDataSourceImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : ImageSearchPreferencesDataSource {
    private val DELETED_IMAGE_IDS = stringSetPreferencesKey("deleted_image_ids")
    private val INDEX_IMAGE = intPreferencesKey("index_image")

    private val imageIds: Flow<List<Long>> = context.dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[DELETED_IMAGE_IDS]?.map {
                it.toLong()
            } ?: emptyList()
        }

    private val indexImage: Flow<Int> = context.dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[INDEX_IMAGE] ?: -1
        }


    override fun getDeletedImages(): Flow<List<Long>> = imageIds

    override suspend fun deleteImage(image: Image) {
        context.dataStore.edit { prefs ->
            val imageIds = prefs[DELETED_IMAGE_IDS]?.toMutableSet() ?: mutableSetOf()
            imageIds.add(image.id.toString())
            prefs[DELETED_IMAGE_IDS] = imageIds
        }
    }

    override fun getIndexImage(): Flow<Int> = indexImage

    override suspend fun setIndexImage(index: Int) {
        context.dataStore.edit { prefs ->
            prefs[INDEX_IMAGE] = index
        }
    }


}