package com.golfzonvn.app.imagesearch.core.datastore.di

import com.golfzonvn.app.imagesearch.core.datastore.ImageSearchPreferencesDataSource
import com.golfzonvn.app.imagesearch.core.datastore.ImageSearchPreferencesDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DataStoreModule {

    @Binds
    fun provideImageSearchDataStore(
        abc: ImageSearchPreferencesDataSourceImpl
    ): ImageSearchPreferencesDataSource
}