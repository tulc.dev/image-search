package com.golfzonvn.app.imagesearch.core.model

data class Image(
    val id: Long,
    val photographer: String?,
    val url: String?,
    val thumbnail: String?,
)