package com.golfzonvn.app.imagesearch.core.network

import com.golfzonvn.app.imagesearch.core.network.model.ImageNetworkResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ImageSearchNetworkDataSource {
    suspend fun getImages(query: String, page: Int = 1, perPage: Int = 15): ImageNetworkResponse
}
