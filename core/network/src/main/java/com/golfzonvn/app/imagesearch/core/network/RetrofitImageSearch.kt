package com.golfzonvn.app.imagesearch.core.network

import com.golfzonvn.app.imagesearch.core.network.model.ImageNetworkResponse
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.Call
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject
import javax.inject.Singleton

interface RetrofitImageSearchApi {

    @GET(value = "v1/search")
    suspend fun getImages(
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int = 15,
    ): ImageNetworkResponse

}

private const val ImageSearchBaseUrl = BuildConfig.BACKEND_URL


@Singleton
class RetrofitImageSearchNetwork @Inject constructor(
    networkJson: Json,
    okhttpCallFactory: Call.Factory,
) : ImageSearchNetworkDataSource {

    private val networkApi = Retrofit.Builder()
        .baseUrl(ImageSearchBaseUrl)
        .callFactory(okhttpCallFactory)
        .addConverterFactory(
            @OptIn(ExperimentalSerializationApi::class)
            networkJson.asConverterFactory("application/json".toMediaType()),
        )
        .build()
        .create(RetrofitImageSearchApi::class.java)

    override suspend fun getImages(query: String, page: Int, perPage: Int): ImageNetworkResponse =
        networkApi.getImages(query, page, perPage)
}