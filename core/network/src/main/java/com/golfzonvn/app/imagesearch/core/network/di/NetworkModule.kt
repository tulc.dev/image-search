package com.golfzonvn.app.imagesearch.core.network.di

import com.golfzonvn.app.imagesearch.core.network.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.Call
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun providesNetworkJson() : Json = Json {
        ignoreUnknownKeys = true
    }


    @Provides
    @Singleton
    fun okHttpCallFactory(): Call.Factory = OkHttpClient.Builder()
        .addInterceptor(
            HttpLoggingInterceptor()
                .apply {
                    if (BuildConfig.DEBUG) {
                        setLevel(HttpLoggingInterceptor.Level.BODY)
                    }
                },
        )
        .addInterceptor(Interceptor {chain ->
            val builder = chain.request().newBuilder()
            builder.addHeader("Authorization", BuildConfig.PEXELS_API_KEY)
            builder.addHeader("X-Ratelimit-Limit", "20000")
            builder.addHeader("X-Ratelimit-Remaining", "19684")
            builder.addHeader("X-Ratelimit-Reset", "1590529646")

            chain.proceed(builder.build())
        })
        .build()



}