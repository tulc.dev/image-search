package com.golfzonvn.app.imagesearch.core.network.model

import kotlinx.serialization.Serializable

@Serializable
data class ImageNetwork(
    val id: Long = 0L,
    val photographer: String = "",
    val src: ImageSourceNetwork? = null,
)