package com.golfzonvn.app.imagesearch.core.network.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class ImageNetworkResponse(
    val page: Int? = null,
    val per_page: Int? = null,
    val total_results: Int? = null,
    val next_page: String? = null,
    val photos: List<ImageNetwork>? = null,
)