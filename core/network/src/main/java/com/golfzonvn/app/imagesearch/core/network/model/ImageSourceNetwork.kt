package com.golfzonvn.app.imagesearch.core.network.model

import kotlinx.serialization.Serializable

@Serializable
data class ImageSourceNetwork(
    val id: Long = 0L,
    val original: String = "",
    val large2x: String = "",
    val large: String = "",
    val medium: String = "",
    val small: String = "",
    val portrait: String = "",
    val landscape: String = "",
    val tiny: String = "",
)
