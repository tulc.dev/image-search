package com.golfzonvn.app.imagesearch.feature.search.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.golfzonvn.app.imagesearch.feature.search.databinding.FragmentImageDetailBinding
import com.golfzonvn.app.imagesearch.feature.search.detail.adapter.ImageDetailAdapter
import com.golfzonvn.app.imagesearch.feature.search.search.adapter.ImageAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageDetailFragment : Fragment() {

    private val viewModel: ImageDetailViewModel by viewModels()

    private lateinit var binding: FragmentImageDetailBinding
    private val adapter: ImageDetailAdapter by lazy { ImageDetailAdapter() }

    private val onPageChangeCallback by lazy {
        object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewModel.changePosition(position)
            }
        }
    }

    private var _initPosition: Int = -1
    private fun getInitPosition(): Int {
        if (_initPosition >= 0) return -1
        _initPosition = if (arguments == null) {
            0
        } else {
            ImageDetailFragmentArgs.fromBundle(requireArguments()).position
        }

        return _initPosition
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        container ?: return null
        binding = FragmentImageDetailBinding.inflate(
            LayoutInflater.from(container.context),
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.vpImage.adapter = adapter

        binding.vpImage.registerOnPageChangeCallback(onPageChangeCallback)

        binding.btnBack.setOnClickListener { findNavController().popBackStack() }

        viewModel.images.observe(viewLifecycleOwner) { list ->
            adapter.setData(list)

            val pos = getInitPosition()
            if (pos >= 0) {
                binding.vpImage.setCurrentItem(pos, false)
            }
        }

        binding.fbDelete.setOnClickListener {
            val item = adapter.getItem(binding.vpImage.currentItem)

            viewModel.delete(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.vpImage.unregisterOnPageChangeCallback(onPageChangeCallback)
    }

}