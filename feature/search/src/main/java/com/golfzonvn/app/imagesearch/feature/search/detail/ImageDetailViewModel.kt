package com.golfzonvn.app.imagesearch.feature.search.detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.golfzonvn.app.imagesearch.core.data.repository.ImageRepository
import com.golfzonvn.app.imagesearch.core.data.repository.UserDataRepository
import com.golfzonvn.app.imagesearch.core.model.Image
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ImageDetailViewModel @Inject constructor(
    private val imageRepository: ImageRepository,
    private val userDataRepository: UserDataRepository,
) : ViewModel() {

    private val _images = MutableLiveData<List<Image>>()
    val images: LiveData<List<Image>> = _images


    init {
        viewModelScope.launch {
            imageRepository.getImages().collect { list ->
                _images.postValue(list)
            }
        }
    }

    fun delete(item: Image?) {
        item ?: return
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                imageRepository.removeImage(item)
            }
        }

    }

    fun changePosition(pos: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                userDataRepository.setIndexImage(pos)
            }
        }
    }

}