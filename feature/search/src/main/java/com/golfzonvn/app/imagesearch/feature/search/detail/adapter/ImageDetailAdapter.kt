package com.golfzonvn.app.imagesearch.feature.search.detail.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.golfzonvn.app.imagesearch.core.model.Image
import com.golfzonvn.app.imagesearch.feature.search.databinding.ItemImageBinding
import com.golfzonvn.app.imagesearch.feature.search.databinding.ItemImageDetailBinding
import javax.inject.Inject

class ImageDetailAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val images = mutableListOf<Image>()

    fun setData(images: List<Image>) {
        this.images.clear()
        this.images.addAll(images)
        this.notifyDataSetChanged()
    }

    fun getItem(position: Int) = images.getOrNull(position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ImageViewHolder.newInstance(parent)
    }

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? ImageViewHolder)?.bindTo(images.getOrNull(position))
    }

    class ImageViewHolder(private val binding: ItemImageDetailBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindTo(image: Image?) {
            binding.image = image
        }

        companion object {
            fun newInstance(parent: ViewGroup) = ImageViewHolder(
                ItemImageDetailBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }
}