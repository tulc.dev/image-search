package com.golfzonvn.app.imagesearch.feature.search.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.golfzonvn.app.imagesearch.feature.search.databinding.FragmentImageSearchBinding
import com.golfzonvn.app.imagesearch.feature.search.search.adapter.ImageAdapter
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ImageSearchFragment : Fragment() {

    private val viewModel: ImageSearchViewModel by viewModels()

    private lateinit var binding: FragmentImageSearchBinding
    private val adapter: ImageAdapter by lazy { ImageAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        container ?: return null
        binding = FragmentImageSearchBinding.inflate(
            LayoutInflater.from(container.context),
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvImage.adapter = adapter

        adapter.setOnItemClickListener { position, _ ->
            findNavController().navigate(
                ImageSearchFragmentDirections.actionImageSearchFragmentToImageDetailFragment(
                    position
                )
            )
        }

        adapter.setOnSeeMoreClickListener {
            viewModel.nextPage()
        }

        binding.edSearch.doAfterTextChanged { value ->
            val query = value?.toString()?.trim() ?: ""
            viewModel.search(query)
        }

        viewModel.eventScrollToPosition.observe(viewLifecycleOwner) { position ->
            if (position < 0) return@observe
            (binding.rvImage.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(
                position,
                0
            )

            binding.rvImage.postDelayed({
                viewModel.clearPosition()
            }, 100)
        }

        viewModel.uiState.observe(viewLifecycleOwner) { data ->
            adapter.setLoading(data.isLoading)
            adapter.setLoaded(data.isLoaded)
            adapter.setData(data.items)

            if (!data.message.isNullOrBlank()) {
                Snackbar.make(binding.root, data.message ?: "", Snackbar.LENGTH_LONG).show()
            }
        }
    }

}