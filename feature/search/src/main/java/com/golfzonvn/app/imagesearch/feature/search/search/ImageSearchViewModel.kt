package com.golfzonvn.app.imagesearch.feature.search.search

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.*
import com.golfzonvn.app.imagesearch.core.data.repository.ImageRepository
import com.golfzonvn.app.imagesearch.core.data.repository.UserDataRepository
import com.golfzonvn.app.imagesearch.core.model.Image
import com.golfzonvn.app.imagesearch.core.network.model.ImageNetworkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import com.golfzonvn.app.imagesearch.core.common.model.Result
import com.golfzonvn.app.imagesearch.core.common.model.asResult


data class ImageSearchUiState(
    val query: String = "",
    val isLoading: Boolean = false,
    val isLoaded: Boolean = false,
    val items: List<Image> = emptyList(),
    val message: String? = null,
    val isConnecting: Boolean = true
)

@HiltViewModel
class ImageSearchViewModel @Inject constructor(
    private val imageRepository: ImageRepository,
    private val userDataRepository: UserDataRepository,
) : ViewModel() {

    private var currentQuery: String = ""
    private var currentPage: Int = 0

    val eventScrollToPosition: LiveData<Int> = userDataRepository.getIndexImage().asLiveData()

    val query = MutableStateFlow<String>("")

    private val imageNetworkResponse = MutableStateFlow<Result<ImageNetworkResponse>?>(null)

    private val _handler by lazy {
        Handler(Looper.myLooper() ?: Looper.getMainLooper())
    }

    private val _uiState: Flow<ImageSearchUiState> = combine(
        query,
        imageNetworkResponse,
        imageRepository.getImages()

    ) { query, response, images ->
        if (query.length <= 2) {
            ImageSearchUiState(
                query = query,
                isLoading = false,
                isLoaded = true,
                items = emptyList(),
            )
        } else {
            val loading = response == Result.Loading

            when (response) {
                is Result.Loading -> {
                    ImageSearchUiState(
                        query = query,
                        isLoading = loading,
                        isLoaded = false,
                        items = images
                    )
                }
                is Result.Success -> {
                    response.data.page?.let { page ->
                        this.currentPage = page
                    }

                    ImageSearchUiState(
                        query = query,
                        isLoading = loading,
                        isLoaded = !loading && ((response.data.page ?: 0) * (response.data.per_page
                            ?: 0)) >= (response.data.total_results ?: 0),
                        items = images,
                    )
                }
                else -> {
                    ImageSearchUiState(
                        query = query,
                        isLoading = false,
                        isLoaded = true,
                        items = images,
                        message = (response as? Result.Error)?.exception?.message
                    )
                }
            }
        }
    }

    val uiState = _uiState.asLiveData()

    init {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                imageRepository.deleteAllImage()
            }
        }
    }

    fun search(query: String, delay: Long = 300L) {
        if (query == currentQuery) return

        this.currentPage = 1
        this.currentQuery = query

        if (query.length <= 2) {
            viewModelScope.launch {
                withContext(Dispatchers.IO) {
                    imageRepository.deleteAllImage()
                }
            }
            return
        }

        _handler.postDelayed(
            { processSearch(query, 1) }, delay
        )
    }

    private fun processSearch(query: String, page: Int) {
        if (query != currentQuery) return
        viewModelScope.launch {
            imageRepository.fetchImages(query, page = page).asResult().collect {
                when (it) {
                    is Result.Loading -> {
                        imageNetworkResponse.value = it
                    }
                    is Result.Success -> {
                        imageNetworkResponse.value = it
                    }

                    else -> {
                        imageNetworkResponse.value = it
                    }
                }

            }
        }
    }

    fun nextPage() {
        processSearch(currentQuery, currentPage + 1)
    }

    fun clearPosition() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                userDataRepository.setIndexImage(-1)
            }
        }
    }

}