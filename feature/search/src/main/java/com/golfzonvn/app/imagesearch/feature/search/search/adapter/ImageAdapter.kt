package com.golfzonvn.app.imagesearch.feature.search.search.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.golfzonvn.app.imagesearch.core.model.Image
import com.golfzonvn.app.imagesearch.feature.search.databinding.ItemImageBinding
import com.golfzonvn.app.imagesearch.feature.search.databinding.ItemImageFooterBinding

class ImageAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val _images = mutableListOf<Image>()
    private var _onItemClickListener: ((position: Int, image: Image?) -> Unit)? = null
    private var _onSeeMoreClickListener: (() -> Unit)? = null
    private var _loading: Boolean = false
    private var _loaded: Boolean = false

    fun setData(images: List<Image>) {
        this._images.clear()
        this._images.addAll(images)
        this.notifyDataSetChanged()
    }

    fun setLoaded(loaded: Boolean) {
        _loaded = loaded
        this.notifyDataSetChanged()
    }

    fun setLoading(loading: Boolean) {
        _loading = loading
        this.notifyDataSetChanged()
    }

    fun setOnItemClickListener(listener: (position: Int, image: Image?) -> Unit) {
        _onItemClickListener = listener
    }

    fun setOnSeeMoreClickListener(listener: () -> Unit) {
        _onSeeMoreClickListener = listener
    }

    override fun getItemViewType(position: Int): Int {
        if (position >= _images.size) {
            return VIEW_TYPE_FOOTER
        }
        return VIEW_TYPE_IMAGE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_IMAGE) {
            return ImageViewHolder.newInstance(parent)
        }

        return FooterViewHolder.newInstance(parent)
    }

    override fun getItemCount(): Int = (_images.size + 1)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? ImageViewHolder)?.bindTo(position, _images.getOrNull(position)) {
            _onItemClickListener?.invoke(position, _images.getOrNull(position))
        }
        (holder as? FooterViewHolder)?.bindTo(_loading, _loaded) {
            _onSeeMoreClickListener?.invoke()
        }
    }

    class ImageViewHolder(private val binding: ItemImageBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindTo(position: Int, image: Image?, onClick: () -> Unit) {
            binding.image = image
            binding.root.setOnClickListener {
                onClick.invoke()
            }
        }

        companion object {
            fun newInstance(parent: ViewGroup) = ImageViewHolder(
                ItemImageBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }
    }

    class FooterViewHolder(private val binding: ItemImageFooterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindTo(loading: Boolean, loaded: Boolean, onClick: () -> Unit) {
            binding.tvSeeMore.visibility = if (!loading && !loaded) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
            binding.pbLoading.visibility = if (loading && !loaded) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
            binding.root.setOnClickListener {
                onClick.invoke()
            }
            binding.executePendingBindings()
        }

        companion object {
            fun newInstance(parent: ViewGroup) = FooterViewHolder(
                ItemImageFooterBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }
    }

    companion object {
        const val VIEW_TYPE_IMAGE = 1
        const val VIEW_TYPE_FOOTER = 2
    }
}