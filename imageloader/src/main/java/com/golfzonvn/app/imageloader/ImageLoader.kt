package com.golfzonvn.app.imageloader

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import java.net.HttpURLConnection
import java.net.URL

class ImageLoader private constructor(private val context: Context) {
    private var _url: String? = null
    private var _placeholder: Int? = R.mipmap.image_placeholder

    fun load(url: String): ImageLoader = apply {
        _url = url
    }

    private fun setBitmap(view: ImageView, bm: Bitmap) {
        if (_mapView[view] == _url) {
            view.post {
                view.setImageBitmap(bm)
            }
        }
        _mapView.remove(view)
    }

    fun placeholder(value: Int? = null): ImageLoader = apply {
        _placeholder = value
    }

    fun into(view: ImageView) {
        _url ?: return
        _mapView[view] = _url ?: ""

        val placeholder = if (_placeholder == null || _placeholder == 0) {
            null
        } else {
            ContextCompat.getDrawable(view.context, _placeholder!!)
        }


        ImageLoadingProgress.getInstance(context).process(_url ?: "", view, placeholder) { bmp ->
            if (bmp == null) {
                return@process
            }
            val width = view.measuredWidth
            val height = view.measuredHeight
            if (width > 0 && bmp.width > width) {
                val newBitmap =
                    Bitmap.createScaledBitmap(bmp, width, width * bmp.height / bmp.width, true)
                setBitmap(view, newBitmap)
            } else {
                setBitmap(view, bmp)
            }
        }
    }

    companion object {
        private val _mapView = mutableMapOf<View, String>()

        fun with(context: Context) = ImageLoader(context)
        fun with(view: View) = ImageLoader(view.context)
    }
}