package com.golfzonvn.app.imageloader

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.util.Log
import android.util.LruCache
import android.view.View
import android.widget.ImageView
import java.net.HttpURLConnection
import java.net.URL

class ImageLoadingProgress constructor(val context: Context) {
    private val _viewThreads = mutableMapOf<View, Thread>()
    private val _threads = mutableMapOf<String, Thread>()

    private val memoryCache: LruCache<String, Bitmap> by lazy {
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()

        // Use 1/8th of the available memory for this memory cache.
        val cacheSize = maxMemory / 8

        object : LruCache<String, Bitmap>(cacheSize) {
            override fun sizeOf(key: String, bitmap: Bitmap): Int {
                // The cache size will be measured in kilobytes rather than
                // number of items.
//                return bitmap.byteCount / 1024
                return 0
            }
        }
    }

    private fun getBitmapFromMemoryCache(key: String) = memoryCache.get(key)

    private fun addBitmapToMemoryCache(key: String, bm: Bitmap) {
        if (bm.width == 0 || bm.height == 0) return
        val w = (512 * context.resources.displayMetrics.density).toInt()

        if (bm.width < w) {
            memoryCache.put(key, bm)
            return
        }
        val h = w * bm.height / bm.width
        val thumbnail = Bitmap.createScaledBitmap(bm, w, h, true)
        memoryCache.put(key, thumbnail)
    }


    fun process(imageUrl: String, view: ImageView, placeholder: Drawable?, callback: (Bitmap?) -> Unit) =
        synchronized("image-progress") {
            if (_viewThreads[view] != null) {
                _viewThreads[view]?.interrupt()
            }

            if (_threads.containsKey(imageUrl)) {
                _threads[imageUrl]?.interrupt()
            }

            val cache = getBitmapFromMemoryCache(imageUrl)
            if (cache != null) {
                view.setImageBitmap(cache)
            } else {
                view.setImageDrawable(placeholder)
            }

            val thread = Thread {
                synchronized("thread-image-progress") {
                    try {
                        val url = URL(imageUrl)
                        val con: HttpURLConnection =
                            url.openConnection() as? HttpURLConnection ?: return@Thread
                        con.doInput = true
                        con.connect()
                        val responseCode = con.responseCode
                        if (responseCode == HttpURLConnection.HTTP_OK) {
                            val inp = con.inputStream
                            val bmp = BitmapFactory.decodeStream(inp)
                            inp.close()

                            addBitmapToMemoryCache(imageUrl, bmp)
                            callback(bmp)
                        }
                    } catch (ex: Exception) {
                        Log.e("Exception", ex.toString())
                        callback.invoke(null)
                    }

                    _viewThreads.remove(view)
                    _threads.remove(imageUrl)
                }
            }

            _viewThreads[view] = thread
            _threads[imageUrl] = thread

            thread.start()
        }


    companion object {
        @Volatile
        @SuppressLint("StaticFieldLeak")
        private var _instance: ImageLoadingProgress? = null

        fun getInstance(context: Context): ImageLoadingProgress = _instance ?: synchronized(this) {
            _instance = ImageLoadingProgress(context.applicationContext)
            _instance!!
        }
    }
}