pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()

        maven(url= "https://jitpack.io")
    }
}
rootProject.name = "Image Search"
include(":app")
include(":core:model")
include(":core:network")
include(":core:datastore")
include(":core:common")
include(":feature:search")
include(":core:data")
include(":core:database")
include(":imageloader")
